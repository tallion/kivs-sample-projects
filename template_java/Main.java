import java.util.*;

public class Main {
	private static String debug_prefix = "DEBUG: ";
	private static String solution_prefix = "SOLUTION: ";

	public static void logDebug(String str) {
		System.out.println(debug_prefix + str);
	}

	public static void logSolution(String str) {
		System.out.println(solution_prefix + str);
	}

	public static void logSolution(int number) {
		System.out.println(solution_prefix + String.valueOf(number));
	}

	public static void main(String[] argv) {
		Scanner scanner = new Scanner(System.in);

		if (argv.length < 2) {
			System.out.println("Command parameter missing.");
			System.exit(1);
		}
		switch (argv[0]) {
			case "example": {
				// Hier die Programmlogik
				break;
			}
			default: {
				System.out.println("Invalid command: '" + argv[0] + "'");
				break;
			}
		}
	}

}

